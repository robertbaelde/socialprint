<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>LUNA | Responsive Admin Theme</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/font-awesome.css') }}"/>
    <link rel="stylesheet" href="{{ asset('vendor/animate.css/animate.css') }}"/>
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.css') }}"/>
    <link rel="stylesheet" href="{{ asset('vendor/toastr/toastr.min.css') }}"/>


    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset('styles/pe-icons/pe-icon-7-stroke.css') }}"/>
    <link rel="stylesheet" href="{{ asset('styles/pe-icons/helper.css') }}"/>
    <link rel="stylesheet" href="{{ asset('styles/stroke-icons/style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('styles/style.cs') }}s">
</head>
<body class="blank">

<!-- Wrapper-->
<div class="wrapper">


    <!-- Main content-->
    <section class="content">
       <!--  <div class="back-link">
            <a href="index.html" class="btn btn-accent">Back to Dashboard</a>
        </div> -->

        <div class="container-center animated slideInDown">


            <div class="view-header">
                <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-unlock"></i>
                </div>
                <div class="header-title">
                    <h3>Login</h3>
                    <small>
                        Please enter your credentials to login. {{ $errors->first() }}
                    </small>
                </div>
            </div>

            <div class="panel panel-filled">
                <div class="panel-body">
                    <form method="POST" action="/auth/login" id="loginForm" novalidate>
                        {!! csrf_field() !!}

                        
                         <div class="form-group">
                            <label class="control-label" for="email">Email</label>
                            <input type="email" placeholder="example@gmail.com" title="Please enter you email" required="" value="{{ old('email') }}" name="email" id="email" class="form-control">
                            <span class="help-block small">Your unique email</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="password">Password</label>
                            <input type="password" title="Please enter your password" placeholder="******" required="" value="" name="password" id="password" class="form-control">
                            <span class="help-block small">Your strong password</span>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-accent">Login</button>
                            <a class="btn btn-default" href="{{url('auth/register')}}">Register</a>
                        </div>
                    </form>
                   <!--  <form action="index.html" id="loginForm" novalidate>
                        <div class="form-group">
                            <label class="control-label" for="username">Username</label>
                            <input type="text" placeholder="example@gmail.com" title="Please enter you username" required="" value="" name="username" id="username" class="form-control">
                            <span class="help-block small">Your unique username to app</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="password">Password</label>
                            <input type="password" title="Please enter your password" placeholder="******" required="" value="" name="password" id="password" class="form-control">
                            <span class="help-block small">Your strong password</span>
                        </div>
                        <div>
                            <button class="btn btn-accent">Login</button>
                            <a class="btn btn-default" href="register.html">Register</a>
                        </div>
                    </form> -->
                </div>
            </div>

        </div>
    </section>
    <!-- End main content-->

</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
<script src="{{ asset('vendor/pacejs/pace.min.js') }}"></script>
<script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/toastr/toastr.min.js')}}"></script>

<!-- App scripts -->
<script src="{{ asset('scripts/luna.js') }}"></script>
<script type="text/javascript">
 $(document).ready(function () {
       

        toastr.options = {
            "debug": false,
            "newestOnTop": false,
            "positionClass": "toast-bottom-right",
            "closeButton": true,
            "progressBar": true
        };
        @foreach ($errors->all() as $message) 
            toastr.error('{{$message}}');
        @endforeach

    });
 </script>
</body>

</html>