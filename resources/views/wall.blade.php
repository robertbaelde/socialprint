<!DOCTYPE html>
<html>
<head>
	<title>wall</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <!-- Page title -->
    <title>wall</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="{{ asset('wall_assets/style.css') }}"/>
</head>
<body>
	<div class="wall_container" id="wall_container">
		<div class="grid">
		  	<div class="grid-sizer">
		  	</div>
			<!-- <div class="grid-item">
				<img class="grid_image" src="http://lorempixel.com/400/400/nightlife">
			</div>
			<div class="grid-item">
				<img class="grid_image" src="http://lorempixel.com/400/400/nightlife?a">
			</div>
			<div class="grid-item twice">
				<img class="grid_image" src="http://lorempixel.com/400/400/nightlife?b">
			</div>
			<div class="grid-item">
				<img class="grid_image" src="http://lorempixel.com/400/400/nightlife?c">
			</div>
			<div class="grid-item twice">
				<img class="grid_image" src="http://lorempixel.com/400/400/nightlife?d">
			</div> -->
 
		  
		  
		</div>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
	<script src="https://npmcdn.com/masonry-layout@4.0/dist/masonry.pkgd.min.js"></script>
	<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
	<script type="text/javascript">
	
	$(document).ready(function () {
		$('.wall_container').click(function(e){
			e.preventDefault();
			var elem = document.getElementById("wall_container");
			fullscreen(elem);
		});

		// $('.grid').masonry({
		//   itemSelector: '.grid-item',
		//   columnWidth: '.grid-sizer',
		//   percentPosition: true
		// });
		


		// add image
		$('.append-button').on( 'click', function() {
			// x
		});

		
	});
	// init Masonry
		var $grid = $('.grid').masonry({
		  // options...
		});
		// layout Masonry after each image loads
		$grid.imagesLoaded().progress( function() {
		  $grid.masonry('layout');
		});
	function addmultiple()
	{
		  var elems = [ getItemElement(), getItemElement(), getItemElement() ];
		  // make jQuery object
		  var $elems = $( elems );
		  $grid.append( $elems ).masonry( 'appended', $elems );
		  $grid.imagesLoaded().progress( function() {
			  $grid.masonry('layout');
			});
	}
	function add()
	{
		  var elems =getItemElement()
		  // make jQuery object
		  var $elems = $( elems );
		  $grid.prepend( $elems ).masonry( 'prepended', $elems );
		  $grid.imagesLoaded().progress( function() {
			  $grid.masonry('layout');
			});
	}
	function addLarge()
	{
		var elem = document.createElement('div');
		var image = document.createElement('img');
		elem.className = 'grid-item twice';
		image.src = "http://lorempixel.com/400/400/nightlife?d="+Math.random();
		elem.appendChild(image);
		var $elems = $( elem );
		$grid.prepend( $elems ).masonry( 'prepended', $elems );
		$grid.imagesLoaded().progress( function() {
			$grid.masonry('layout');
		});
	}
	function getItemElement()
	{
		// var img = $('<img class="grid_image">'); //Equivalent: $(document.createElement('img'))
		// img.attr('src', "http://lorempixel.com/400/400/nightlife?d="+Math.random());
		// return img;
		// var $div = $("<div>", {class: "grid-item"});
		var elem = document.createElement('div');
		// if(random() == 1){
			elem.className = 'grid-item ';
		// }
		// else{
		// 	elem.className = 'grid-item twice';
		// }
		var image = document.createElement('img');
		image.src = "http://lorempixel.com/400/400/nightlife?d="+Math.random();
		elem.appendChild(image);
		return elem;
	}
	function random()
	{
	return Math.floor(Math.random()*2+1)  
	}
	function fullscreen(elem)
	{
		if (elem.requestFullscreen) {
		  elem.requestFullscreen();
		} else if (elem.msRequestFullscreen) {
		  elem.msRequestFullscreen();
		} else if (elem.mozRequestFullScreen) {
		  elem.mozRequestFullScreen();
		} else if (elem.webkitRequestFullscreen) {
		  elem.webkitRequestFullscreen();
		}
	}
	</script>

</body>
</html>