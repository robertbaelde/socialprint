@extends('dashboard.template')

@section('title', 'Devices')
@section('header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css"/>

	
@endsection
@section('content')
<div class="row">
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-filled">
					<div class="panel-heading">
						<div class="panel-tools">
							<a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
						</div>
						<h3>{{$device->name}}</h3>
					</div>
					<div class="panel-body">				
						<dl class="dl-horizontal">
							<dt>Key</dt>
							<dd>{{$device->key}}</dd>

							<dt>status</dt>
							<dd>{{$device->status}}</dd>

							<dt>note</dt>
							<dd>{{$device->note}}</dd>
						</dl>
						<a class="btn btn-default  pull-right" href="{{url('admin/device/'.$device->id.'/edit')}}">Edit</a>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-filled">
					<div class="panel-heading">
						<div class="panel-tools">
							<a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
						</div>
						<h3>Make reservation for user</h3>
					</div>
					<div class="panel-body">				
						<form class="form-horizontal" method="POST" action="{{url('admin/device/'.$device->id.'/reservation')}}" >
                		{!! csrf_field() !!}
	                	<div class="form-group">
	                        <label class="col-sm-2 control-label" >User</label>
	                        <div class="col-sm-10">
	                        	<select class="form-control" name="user_id">
			                       	@foreach(App\User::get() as $user)
		                        		<option value="{{$user->id}}">{{$user->name}}</option>
			                       	@endforeach
			                    </select>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                   	 	<label class="col-sm-2 control-label" >Date range</label>
	                        <div class="col-sm-10">
	                    		<div class="input-daterange input-group" id="datepicker">
						    		<input type="text" class="input-sm form-control" name="from" />
						    		<span class="input-group-addon">to</span>
						   			<input type="text" class="input-sm form-control" name="to" />
								</div>
							</div>
						</div>
						<div class="form-group">
	                        <label class="col-sm-2 control-label" >Note</label>
	                        <div class="col-sm-10">
	                        	<textarea class="form-control" rows="3" name="note" placeholder=""></textarea>	
	                        </div>
	                    </div>
                    	<button type="submit" class="btn btn-default  pull-right">reserve</button>
                		</form>
					</div>
				</div>
			</div>
		</div>
		

	</div>
	<div class="col-md-8">
		<div class="panel">
			<div class="panel-heading">
				<div class="panel-tools">
					<a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
				</div>
				Timeline
			</div>
			<div class="panel-body">
				<div class="v-timeline vertical-container">
					@each('dashboard.partials.activity', $device->activity, 'activity')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('.input-daterange').datepicker({
			format: "dd-mm-yyyy",
		    language: "nl",
		    autoclose: true,
		});
	});
</script>
@endsection

