<div class="vertical-timeline-block">
	<div class="vertical-timeline-icon">
		<i class="{{$activity->icon}}"></i>
	</div>
	<div class="vertical-timeline-content">
		<div class="p-sm">
			<span class="vertical-date pull-right"> {{$activity->created_at->format('l')}}<br> <small>{{$activity->created_at->toDateTimeString()}}</small> </span>
			<h2>{{$activity->title}}</h2>
			<p>{{$activity->body}}</p>
		</div>
		<div class="panel-footer">
			<p>{{$activity->footer}}</p>
		</div>
	</div>
</div>