@extends('dashboard.template')

@section('title', 'setups')

@section('content')
<div class="row">
	<div class="col-xs-12">
	
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-filled">
			<div class="panel-heading">
				<div class="panel-tools">
					<a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
					<!-- <a class="panel-close"><i class="fa fa-times"></i></a> -->
				</div>
				DataTables with HTML5 export buttons
			</div>
			<div class="panel-body">
				<p>
					
				</p>
				<div class="table-responsive">
					<table id="setups" class="table table-striped table-hover">
						<thead>
						<tr>
							<th>name</th>
							<th>note</th>
							<th>created at</th>
							<th>actions</th>
						</tr>
						</thead>
						<tbody>
							@foreach($setups as $setup)
								<tr>
									<td>{{$setup->name}}</td>
									<td>{{$setup->note}}</td>
									<td>{{$setup->created_at->toDateString()}}</td>
									<td>
										<a href="{{url('setup/'.$setup->id)}}">view</a> | 
										<a href="{{url('setup/'.$setup->id.'/edit')}}">edit</a> | 
										<a href="javascript:deletesetup('{{ $setup->id }}');">delete</a></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>
   
@endsection

@section('script')
<script type="text/javascript">
	 $(document).ready(function () {

		$('#setups').DataTable({
			dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
			"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
			buttons: [
				{extend: 'copy',className: 'btn-sm'},
				{extend: 'csv',title: 'ExampleFile', className: 'btn-sm'},
				{extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
				{extend: 'print',className: 'btn-sm'}
			],
			"order": [[ 1, "asc" ]],
		});
		
	});
	function deletesetup(id) {
		if (confirm('Delete this setup?')) {
			$.ajax({
				type: "DELETE",
				url: "{{url('setup')}}/"+ id, //resource
				success: function(affectedRows) {
					console.log(affectedRows);
					//if something was deleted, we redirect the user to the setups page, and automatically the user that he deleted will disappear
					if (affectedRows == 'success') window.location = "{{url('setup')}}";
				}
			});
		}
	}
</script>
@endsection