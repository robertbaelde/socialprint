@extends('dashboard.template')

@section('title', 'Setup')
@section('header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css"/>

	
@endsection
@section('content')
<div class="row">
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-filled">
					<div class="panel-heading">
						<div class="panel-tools">
							<a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
						</div>
						<h3>{{$setup->name}}</h3>
					</div>
					<div class="panel-body">				
						<dl class="dl-horizontal">
							<dt>note</dt>
							<dd>{{$setup->note}}</dd>
						</dl>
						<a class="btn btn-default  pull-right" href="{{url('setup/'.$setup->id.'/edit')}}">Edit</a>
						<a class="btn btn-default  pull-right" href="{{url('setup/'.$setup->id.'/instafeed/create')}}">add instagram feed</a>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active">
                            	<a data-toggle="tab" href="#instagram" aria-expanded="true"><i class="fa fa-instagram"></i></a>
                       		</li>
                            <li class=""><a data-toggle="tab" href="#facebook" aria-expanded="false"><i class="fa fa-facebook"></i></a></li>
                            <li class=""><a data-toggle="tab" href="#twitter" aria-expanded="false"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="instagram" class="tab-pane active">
                                <div class="panel-body">
                                    <strong class="c-white">Instagram feeds</strong>
                                    <p>
                                    @foreach($setup->instagramfeed as $instafeed)
                                    <a href="{{url('setup/'.$setup->id.'/instafeed/'.$instafeed->id)}}">{{$instafeed->name}}</a><Br>
                                    @endforeach
                                    </p>                 
                                </div>
                            </div>
                            <div id="facebook" class="tab-pane">
                                <div class="panel-body">
                                    <strong class="c-white">Donec quam felis</strong>                                 
                                </div>
                            </div>
                            <div id="twitter" class="tab-pane">
                                <div class="panel-body">
                                    <strong class="c-white">Donec quasdfm felis</strong>
                                </div>
                            </div>
                        </div>
                    </div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-filled">
					<div class="panel-heading">
						<div class="panel-tools">
							<a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
						</div>
						<h3>Devices</h3>
					</div>
					<div class="panel-body">	
						<table class="table">			
						@foreach($setup->user->devices as $device)
						
							<tr>
								<td><h5>{{$device->name}}</h5></td>
								<td> 
								@if($device->setup_id == $setup->id)
									<a href="{{url('setup/'.$setup->id.'/device/'.$device->id.'/resign')}}" class="btn btn-w-md btn-success btn-squared"><i class="fa fa-check"></i><span class="bold">Assigned</span></a>
								@elseif($device->setup_id !== 0)
									@if($device->setup->user_id == Auth::user()->id)
										Assigned to <a href="{{url('setup/'.$setup->id)}}">{{$device->setup->name}}</a>
									@else 
										Status unknown 
									@endif
								@else 
									<a href="{{url('setup/'.$setup->id.'/device/'.$device->id.'/assign')}}" class="btn btn-w-md btn-primary btn-squared"><i class="fa fa-link"></i> <span class="bold">Assign to this setup</span></a>
								@endif
								</td>
								<td>
								@if($device->setup_id == $setup->id)
									@if($device->status == 'active')
									<a href="{{url('setup/'.$setup->id.'/device/'.$device->id.'/disable')}}" class="btn btn-w-md btn-success btn-squared"><i class="fa fa-check"></i> <span class="bold">Active</span></a>
									@elseif($device->status == 'disabled')
									<a href="{{url('setup/'.$setup->id.'/device/'.$device->id.'/activate')}}" class="btn btn-w-md btn-danger btn-squared"><i class="fa fa-times"></i> <span class="bold">Disabled</span></a>

									@endif
								@endif
								</td>
							</tr>
						@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
		

	</div>
	
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('.input-daterange').datepicker({
			format: "dd-mm-yyyy",
		    language: "nl",
		    autoclose: true,
		});
	});
</script>
@endsection

