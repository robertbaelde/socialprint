 {!!Menu::Render(request())!!}
<?php
/**
* 	Render menu
*/
class Menu
{
	
	protected static $menu = [
		[
			'type' => 'divider',
			'name' => 'Main'
		],
		[
			'type' => 'url',		
			'name' => 'Index',
			'url' => '/'
		],
		[
			'type' => 'list',
			'name' => 'Setups',
			'urls' => [
				[
					'name' => 'All',
					'url' => 'setup'
				],
				[
					'name' => 'Add',
					'url' => 'setup/create'
				]
			]
		]
	];
	protected static $adminmenu = [
		[
			'type' => 'divider',
			'name' => 'Admin'
		],
		[
			'type' => 'list',
			'name' => 'Devices',
			'urls' => [
				[
					'name' => 'All',
					'url' => 'admin/device'
				],
				[
					'name' => 'Add',
					'url' => 'admin/device/create'
				]
			]
		],
	];
	

	
	public static function Render($request)
	{
		$menu = Menu::$menu;
		if(Auth::user()->isAdmin()){
			$menu = array_merge($menu, Menu::$adminmenu);
		}
		$html = '
		<nav>
			<ul class="nav luna-nav">
		';
		
		foreach ($menu as $menuitem) {
			switch ($menuitem['type']) {
				case 'divider':
					$html .= 	'<li class="nav-category">
									'.$menuitem['name'].'
								</li>';
					break;
				case 'url':
					$active = Menu::isActive($request, $menuitem['url']);
					if($active){
						$html .= 	'<li class="active">';

					}
					else{
						$html .= 	'<li class="">';
					}
						$html .= '<a href="'.url($menuitem['url']).'">'.$menuitem['name'].'</a></li>';
					break;
				case 'list':
					$urls = '';
					$active = false;
					foreach ($menuitem['urls'] as $url) {
						if(Menu::isActive($request, $url['url'])){
							$active = true;
							$urls .= '<li class="active"><a href="'.url($url['url']).'">'.$url['name'].'</a></li>';
						}
						else{
							$urls .= '<li><a href="'.url($url['url']).'">'.$url['name'].'</a></li>';
						}
					}

					if(!$active){
						$html .= '<li>
								<a href="#'.$menuitem['name'].'" data-toggle="collapse" aria-expanded="false" class="collapsed">
									'.$menuitem['name'].'<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
								</a>
								<ul id="'.$menuitem['name'].'" class="nav nav-second collapse" aria-expanded="false" style="height: 0px;">';
						$html .= $urls;
						$html .= '</ul></li>';
					}
					else{
						$html .= '<li class="active">
									<a href="#'.$menuitem['name'].'" data-toggle="collapse" aria-expanded="false">
									'.$menuitem['name'].'<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
									</a>
								<ul id="'.$menuitem['name'].'" class="nav nav-second collapse in">';
						$html .= $urls;
						$html .= '</ul></li>';
					}
					break;
				default:
					# code...
					break;
			}
		}
		$html .= '
			</ul>
		</nav>';
		return $html;
	}
	public static function isActive($request, $path)
	{
		return ($request->path() == $path);
	}
}
?>