@extends('dashboard.template')

@section('title', 'Edit Device')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="view-header">
			<div class="pull-right text-right" style="line-height: 14px">
				<small>Device<br><span class="c-white">Edit</span></small>
			</div>
			<div class="header-icon">
				<i class="pe page-header-icon pe-7s-print"></i>
			</div>
			<div class="header-title">
				<h3>Device</h3>
				<small>
					Edit a new device
				</small>
			</div>		
        </div>
		<hr>
	</div>
	<div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                Edit a new device
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{url('device/'.$device->id)}}" >
                 <input type="hidden" name="_method" value="put" />
                {!! csrf_field() !!}
                	 <div class="form-group">
                    	<label for="key" class="col-sm-2 control-label">key</label>
                        <div class="col-sm-10"><input type="text" value="{{$device->key}}" class="form-control" name="key" id="key" placeholder="device 1"></div>
                    </div>
                    <div class="form-group">
                    	<label for="devicename" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10"><input type="text" class="form-control" value="{{$device->name}}" name="name" id="devicename" placeholder="device 1"></div>
                    </div>
                   	<div class="form-group">
                        <label class="col-sm-2 control-label" >Note</label>
                        <div class="col-sm-10">
                        	<textarea class="form-control" rows="3" name="note" placeholder="">{{$device->note}}</textarea>	
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" >Status</label>
                        <div class="col-sm-10">
                        	<select class="form-control" name="status">
		                        <option value="active" @if($device->status == 'active') selected @endif>active</option>
		                        <option value="disabled" @if($device->status == 'disabled') selected @endif >disabled</option>
		                    </select>
                        </div>
                    </div>
                    
                    <button type="submit" class="btn btn-default  pull-right">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection