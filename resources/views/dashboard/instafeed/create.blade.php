@extends('dashboard.template')

@section('title', 'Add Instagram feed')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="view-header">
			<div class="pull-right text-right" style="line-height: 14px">
				<small>Instagram feed<br><span class="c-white">Add</span></small>
			</div>
			<div class="header-icon">
				<i class="pe page-header-icon fa fa-instagram"></i>
			</div>
			<div class="header-title">
				<h3>Instagram feed</h3>
				<small>
					Add a new Instagram feed
				</small>
			</div>
		</div>
		<hr>
	</div>
	<div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                Add a new Instagram feed
   <!--  name
hashtag
lat
lng
locaton_id
method -->
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{url('setup/'.$setup->id.'/instafeed')}}" >
                {!! csrf_field() !!}
                    <div class="form-group">
                    	<label for="instaname" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="name" id="instaname" placeholder=""></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" >Type</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="method">
                                <option value="hashtag">Hashtag</option>
                                <option value="location_lat_lng">Location (lat, lng)</option>
                                <option value="location_id">Instagram location</option>
                            </select>
                        </div>
                    </div>
                    <div class="hashtag">
                        <div class="form-group">
                            <label for="hashtag" class="col-sm-2 control-label">Hashtag</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="hashtag" id="hashtag" placeholder=""></div>
                        </div>
                    </div>

                   	<div class="form-group">
                        <label class="col-sm-2 control-label" >Note</label>
                        <div class="col-sm-10">
                        	<textarea class="form-control" rows="3" name="note" placeholder=""></textarea>	
                        </div>
                    </div>
                    <input type="hidden" name="template_id" value="1">
                   
                    
                    <button type="submit" class="btn btn-default  pull-right">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection