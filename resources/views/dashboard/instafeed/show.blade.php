@extends('dashboard.template')

@section('title', 'Instagram feed details')
@section('header')
  
@endsection
@section('content')
<div class="row">
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-filled">
					<div class="panel-heading">
						<div class="panel-tools">
							<a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
						</div>
						<h3>{{$instafeed->name}}</h3>
					</div>
					<div class="panel-body">				
						<dl class="dl-horizontal">
							<dt>Name</dt>
							<dd>{{$instafeed->name}}</dd>

							
						</dl>
						<a class="btn btn-default  pull-right" href="{{url('setup/'.$setup->id.'/instafeed/'.$instafeed->id.'/edit')}}">Edit</a>
					</div>
				</div>
			</div>
			
		</div>
		

	</div>
	<div class="col-md-8">
		<div class="panel">
			<div class="panel-heading">
				<div class="panel-tools">
					<a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
				</div>
				Latest photos
			</div>
			<div class="panel-body">
				<div class="v-timeline vertical-container">
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')

@endsection

