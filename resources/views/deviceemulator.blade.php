<!DOCTYPE>
<html>
<head>
	<title>Device emulator</title>
	<style type="text/css">
	.container {
		width: 220px;
		display: block;
		margin-left: auto;
		margin-right: auto;
		background-color: grey;
	}
	.photo {
		margin-left: 10px;
		width: 200px;
		height: 300px;
	}
	.controls {
		top: 150px;
		position: absolute;
		/*background-color: green;*/
	}
	.controls input {
		margin-bottom: 10px;
	}


	/*printer thing*/
	#slot{
	    width:220px;
	    height:5px;
	    background-color:black;
	    margin:0px;
	    border-radius:5px;
	    position:relative;
	}
	#paper-holder{
	    position:absolute;
	    overflow:hidden;
	    height:0;
	    top:3px;
	    left:0;
	    right:0;
	   /* -ms-transition:height 15s steps(100,end);
	    -moz-transition:height 15s steps(100,end);
	    -webkit-transition:height 15s steps(100,end);
	    -o-transition:height 15s steps(100,end);
	    transition:height 15s steps(100,end);*/
	     -ms-transition:height 3s;
	    -moz-transition:height 3s;
	    -webkit-transition:height 3s;
	    -o-transition:height 3s;
	    transition:height 3s;
	}
	#paper{
	    position:absolute;
	    width:200px;
	    left:5px;
	    bottom:0;
	    height:300px;
	    line-height:30px;
	    text-align:center;
	    background-color:white;
	    border:1px solid #ccc;
	    box-shadow:2px 2px 2px rgba(0,0,0,0.1);
	    border-top:0;
	}
	#paper-holder.print{
	    height:299px;
	}
	</style>
</head>
<body>
	<div class="container">
		<div class="photo">
			<!-- <img src="" width="200px" id="instaimage"> -->
		</div>
		<div id="slot">
		     <div id="paper-holder">
		         <div id="paper">
		         	<img width="200px" id="instafotorollout" src="/generated/1-setup-1/feeds/2-location/generated_images/L8CWohUm4dCXOd7RpiZjW81WSfljB3.jpg">
		         </div>
		     </div>
		</div>
	</div>
	<div class="controls">
		<br>
		<h3 class="status"></h3>
		<br><Br>
		<select class="selectdevice">
			@foreach(App\Device::get() as $device)
			<option data-key="{{$device->key}}" data-token="{{$device->access_token}}" >{{$device->name}}</option>
			@endforeach
		</select><br><br>
		Device key<br>
		<input type="text" name="key" class="inpt" value="test 123"><br>
		Device token<br>
		<input type="text" name="token" class="inpt" value="test 123"><br>

		<a href="" class="start">Start</a>
		<a href="" class="stop">Stop</a>
		<a href="" class="update">update</a>

		<br>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function () {
		$('.start').click(function(e){
			e.preventDefault();
			Device.start();
		});
		$('.stop').click(function(e){
			e.preventDefault();
			Device.stop();
		});
		$('.selectdevice').change(function(){
		    $("input[name='key']").val($(this).find(':selected').data('key'));
		    $("input[name='token']").val($(this).find(':selected').data('token'));
		});
		$('.update').click(function(e){
			e.preventDefault();
		    Device.update_data();
		});

		Device.init();
	});
	var Device = {
		key: '',
		token: '',
		interval: false,
		status : 'initializing',
		timeout: 2, // secconds 
		printing_id: false,
		init: function(){
			Device.update_data();
			Device.change_status('initializing');
		},
		update_data: function()
		{
			Device.stop();
			Device.key = $("input[name='key']").val();
			Device.token = $("input[name='token']").val();
			Device.change_status('data updated');
		},
		change_status: function(status)
		{
			console.log(status);
			Device.status = status;
			$('.status').html(Device.status);
		},
		loop: function(){
			// console.log(Device.interval);
			
		    clearInterval(Device.interval);
		   	
		   	// do api call
		   	$.ajax({
			  	method: "GET",
			  	url: "{{url('api/v1/device/getjob')}}",
				cache: false,
			  	data: { device_key: Device.key, access_token: Device.token, 'finishd_job_id': Device.printing_id },
			  	beforeSend: function(jqXHR, settings) {
			      	console.log(settings.url);
			  	}
			})

			.done(function( data ) {
				Device.printing_id = false;
				console.log(data);
				if(data.success == false){
					// error handling
					console.log(data.timeout)
				    Device.interval = setInterval(Device.loop, data.timeout*1000);
				}
				else{
					if(data.data.image_url == false){
						Device.change_status('no new image');
						// pull for new
				    	Device.interval = setInterval(Device.loop, data.timeout*1000);
					}
					else{
						Device.change_status('new image');
				    	Device.printing_id = data.data.id; 

						Device.printImage(data.data.image_url);

					}
				}
			});

		},
		start: function(){
			Device.change_status('loop started');
		    clearInterval(Device.interval);

			Device.interval = setInterval(Device.loop, 5);
		},
		stop: function(){
			Device.change_status('loop stoped');

		    clearInterval(Device.interval);

		},
		printImage: function(img_url)
		{
			$('#instaimage').attr("src", img_url);
			$('#instafotorollout').attr("src", img_url);

			$('#paper-holder').show();
			$('#paper-holder').addClass('print');
			setTimeout(function(){
		    	//print completed
		    	$('#paper-holder').hide();
		    	$('#paper-holder').removeClass('print');
		    	$('#instaimage').attr("src", '');
		    	$('#instafotorollout').attr("src", '');

				Device.interval = setInterval(Device.loop, 0.1*1000);



		}, 5*1000);
			// $('#instaimage').hide('slide', {direction: 'down'});
		}
	}
	</script>
</body>
</html>