<!DOCTYPE html>
<html>
<head>
	<title>wall</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <!-- Page title -->
    <title>wall</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="{{ asset('wall_assets/reset.css') }}"/>
    <link rel="stylesheet" href="{{ asset('wall_assets/style2.css') }}"/>
    <link rel="stylesheet" href="{{ asset('wall_assets/vendor/font-awesome/css/font-awesome.min.css')}}">
</head>
<body>
	<div id="header">
		
	</div>
	<div class="wall_container" id="wall_container">
		<!-- <div class="grid">
		  	<div class="grid-sizer">
		  	</div>
			<div class="grid-item">
				<img class="grid_image" src="http://lorempixel.com/400/400/nightlife">
			</div>

		  
		</div> -->
		
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		wall.init();
	});
	$( window ).resize(function() {
		wall.render();
		console.log($( window ).width());
	});
	var wall = {
		min_header_height: 50,
		column_count: 12,
		content : [
			{
				'$div' : false,
				'img_url' : 'http://loremflickr.com/500/500/ultramiami?random='+Math.random(),
				'size_multiplier' : 1, // change to become bigger
				'order_added' : 3,
			},
			{
				'$div' : false,
				'img_url' : 'http://loremflickr.com/500/500/ultramiami?random='+Math.random(),
				'size_multiplier' : 2, // change to become bigger
				'order_added' : 2,
			},
			{
				'$div' : false,
				'img_url' : 'http://loremflickr.com/500/500/ultramiami?random='+Math.random(),
				'size_multiplier' : 1, // change to become bigger
				'order_added' : 4,
			}
		],
		spots: [],
		blockcontainer : $('#wall_container'),
		order_added: 5,
		init: function(){
			var that = this;
			// console.log('initialize');
			that.render();
			setInterval(function(){
				wall.demo_loop();
			}, 5000)
			this.addBlockRandom(20,2,1);
		},
		demo_loop: function()
		{
			// loop to show some events
			random = Math.random();
			if(random < 0.5){
				// add images	
				this.addBlockRandom((Math.floor(Math.random() * 5)),2,1);
			}
			if(random < 0.75){
				// "print" image
				for(i=0; i < Math.floor(Math.random() * 3); i++){
					this.changeBlock(this.content[Math.floor(Math.random() * this.content.length)].order_added, 'print');
				}
				
			}
			if(random >= 0.75){
				// increase image size
				this.changeBlock(this.content[Math.floor(Math.random() * this.content.length)].order_added, 'print');
			}


			// scale random image

		},
		// dev function to add random blocks
		addBlockRandom: function(times, max_size, min_size){
			max_size = max_size || 1;
			min_size = min_size || 1;
			for(i=0; i < times; i++){
				// console.log(i);
				this.addBlock('test', Math.floor(Math.random() * max_size) + min_size)
			}
			return true;
		},
		addBlock: function(url, multiplier)
		{
			if(multiplier == undefined){
				multiplier = 1;
			}
			this.content.push({
				'$div' : false,
				'img_url': 'http://loremflickr.com/250/250/ultramiami?random='+Math.random(),
				'size_multiplier' : multiplier,
				'order_added' : this.order_added++
			});
			this.render();
		},
		changeBlock: function(key, change)
		{
			var result = $.grep(this.content, function(e){ return e.order_added == key; });
			// console.log(result);
			change = 'print';
			if(result.length !== 0){
				if(change == 'print'){
					result[0].$div.addClass('overlay');
					// result[0].printing = true;
					// result[0].multiplier = 2;
					// this.render();
					setTimeout(function(block){ 
							block.$div.removeClass('overlay');
							// block.printing=false;
							// result[0].multiplier = 1;
							wall.render();
						 }, 3000, result[0]);
				}
			}
		},

		render: function()
		{
			this.calculate_grid();

			this.render_header();
			// loop through content,
			// make sure content array is in preffered order (1 => 2 = top left => bottom right)
			this.sort_content_array()
			// reset spots
			this.resetSpots();
			// update blocks
			for (var i = 0; i < this.content.length; i++) {
			    this.update_block(this.content[i]);
			}
		},
		resetSpots: function()
		{
			this.spots = [];
			// Creates all lines:
			for(var i=0; i < this.column_count; i++){
			  // Creates an empty line
			  this.spots.push([]);
			  // Adds cols to the empty line:
			  this.spots[i].push( new Array(this.row_count));
			  for(var j=0; j < this.row_count; j++){
			    // Initializes:
			    this.spots[i][j] = false;
			  }
			}
		},
		calculate_grid: function()
		{
			this.screen = {
				'width' : $( window ).width(),
				'height': $( window ).height(),
			};
			this.block_size = {
				'width' : this.screen.width/this.column_count,
				'height' : this.screen.width/this.column_count,
			};
			// calculate header offset
			this.row_count = Math.floor(this.screen.height/this.block_size.height);
			this.leftover = this.screen.height-(this.row_count*this.block_size.height);
			while(this.leftover < this.min_header_height){
				this.row_count--;
				this.leftover = this.screen.height-(this.row_count*this.block_size.height);
			}
			this.available_spaces = this.row_count*this.column_count;

		},
		render_header: function()
		{
			header_height = this.leftover;
			if(header_height > 50){
				header_height = 50;
			}
			this.header_height = header_height;
			$('#header').css({
				'width' : this.screen.width,
				'height': header_height,
			});
		},
		sort_content_array: function()
		{
			this.content.sort(function(a, b){
				if(a.printing == true){
					return 1;
				}
				return b.order_added - a.order_added;
			});
		},
		update_block: function(block)
		{
			if(block.$div === false){
				block.$div = $('<div>');
				block.$div.addClass('block');
				this.blockcontainer.append(block.$div);

				block.$div.hide();

				$('<div>')
				.html('<i class="fa fa-print fa-5x"></i>')
				.addClass('overlay').appendTo(block.$div);
				// make image
				$("<img />")
					.attr('src', block.img_url)
					.attr('data', block.order_added)
					// wait for image to load, then show it
    				.on('load', function() {
				        if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
				            alert('broken image!');
				        } else {
				        	block.$div.prepend($(this));
				        	block.$div.show();
				        }
		    		});
			}
			
			size_multiplier = block.size_multiplier;		
			while(size_multiplier !== 0){
				block.spot = this.findFreeSpot(size_multiplier); // size multiplier
				// calculate position
				if(block.spot !== false){
					block.top = this.header_height+(block.spot.y)*this.block_size.height;
					block.left = (block.spot.x)*this.block_size.width;
					block.$div.css({
						'top' : block.top,
						'left': block.left,
						'width': this.block_size.width*size_multiplier,
						'height': this.block_size.height*size_multiplier
					});
					// exit
					break;
				}
				// element doesn't fit, try with smaller size
				size_multiplier--;
			}
			if(size_multiplier == 0){
				// doesn't fit, so remove
				block.$div.remove();
				delete block;
			}
			


		},
		findFreeSpot: function(multiplier){
			free = false;

			// loop through rows
			for (var y = 0; y < this.row_count; y++) {
				// loop through columns
				for (var x = 0; x < this.column_count; x++) {
					// check if free
					free = true;
					// console.log('check for x:'+x+',y:'+y+'m:'+multiplier);
					debug_tried = [];

					// for (var m = 0; m < multiplier; m++) {
						// offset = multiplier-1;
						// console.log('offset'+m);
						// check if spots exists on this row/column
						if(this.spots[x+multiplier-1] == undefined){
							free = false;
							break;
						}
						if(this.spots[x+multiplier-1][y+multiplier-1] == undefined){
							free = false;
							break;
						}
						// check if those are free
						for (var ym = y; ym < y+multiplier; ym++) {
							// loop through columns
							for (var xm = x; xm < x+multiplier; xm++) {
								debug_tried.push({'x' : xm, 'y' : ym});

								if(this.spots[xm][ym]){
									free = false;
									break;
								}
							}
							if(free == false){
								break;
							}
						}
						
					// }
					if(free == true){
						// yeah, spot found, lets reserve it
						// console.log('free: x: '+x+' y:'+y);
						reserved = [];
						for (var ym = y; ym < y+multiplier; ym++) {
							// loop through columns
							for (var xm = x; xm < x+multiplier; xm++) {
								// console.log(xm);
								if(this.spots[xm][ym] == true){
									alert('lolwhotmate');
									// console.log('................');
									console.log(debug_tried);
								}
								this.spots[xm][ym] = true;
								reserved.push({
									'x' : xm,
									'y' : ym,
								})
								// console.log('reserved:'+xm+' '+ym);
							}
						}
						return {'x' : x, 'y' : y, 'reserved' : reserved};
					}
				}
				// 
			}
			return false;
		}
	}
	// 12/3 => 4
	// 12/5 => 2 --> 2 left

	</script>
</body>
</html>
