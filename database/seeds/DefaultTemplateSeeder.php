<?php

use App\Template;
use App\TemplateImage;
use App\TemplateText;
use Illuminate\Database\Seeder;

class DefaultTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$template = Template::create([
				'user_id' => 0,
				'name' => 'Default Instagram Template', 
				'note' => 'This is the default template for use with the instagram feed.', 
    		]);
    	$content = $template->content()->create([
    		'name' => 'Instagram Image Large',
			'order' => 1,
    	]);
    	$template_image = TemplateImage::create(
				    			[
				    				'source' => '[[instagram.photo]]', 
				    				'resolution' =>  [
				                        'x' => 600,
				                        'y' => -1, // when y is negative, image ratio is maintained
				                        'mode' => IMG_BILINEAR_FIXED
				                    ], 
				                    'target_location' => [
				                        'x' => 0,
				                        'y' => 0,
				                    ]
				    			]
				    		);
    	// assosiate image with the template content
    	$content->contentable()->associate($template_image)->save();


    	// create profile picture
    	$content = $template->content()->create([
    		'name' => 'Instagram Profile picture',
			'order' => 2,
    	]);
    	$profile_picture = TemplateImage::create(
				    			[
				    				'source' => '[[instagram.user.profilepicture]]', 
				    				'resolution' =>  [
				                        'x' => 100,
				                        'y' => -1, // when y is negative, image ratio is maintained
				                        'mode' => IMG_BILINEAR_FIXED
				                    ], 
				                    'target_location' => [
				                        'x' => 30,
				                        'y' => 790,
				                    ]
				    			]
				    		);
    	// assosiate image with the template content
    	$content->contentable()->associate($profile_picture)->save();

    	// create description text
    	$content = $template->content()->create([
    		'name' => 'Instagram Description text',
			'order' => 3,
    	]);
    	$description_text = TemplateText::create(
				    			[
				    				'text' => '[[instagram.description]]', 
				    				'font_data' =>  [
				                        'font' => storage_path('app/frame/fonts/Lato-Bold.ttf'),
				                        'size' => 28,
				                        'color' => [
				                            'r' => 0,
				                            'g' => 0,
				                            'b' => 0,
				                        ]
				                    ], 
				                    'background_color' => [
				                        'r' => 255,
				                        'g' => 255,
				                        'b' => 255,
				                        'transparant' => false,
				                    ],
				                   	'width' => 600,
                    				'margin' => [
				                        'x' => 30,
				                        'y' => 0,
				                    ],
				                    'target_location' => [
				                        'x' => 0,
				                        'y' => 623,
				                    ]
				    			]
				    		);
    	// assosiate image with the template content
    	$content->contentable()->associate($description_text)->save();

    	// create description text
    	$content = $template->content()->create([
    		'name' => 'Instagram Username text',
			'order' => 4,
    	]);
    	$username_text = TemplateText::create(
				    			[
				    				'text' => '@[[instagram.user.username]]', 
				    				'font_data' =>  [
				                        'font' => storage_path('app/frame/fonts/Lato-Italic.ttf'),
				                        'size' => 20,
				                        'color' => [
				                            'r' => 0,
				                            'g' => 0,
				                            'b' => 0,
				                        ]
				                    ], 
				                    'background_color' => [
				                        'r' => 255,
				                        'g' => 255,
				                        'b' => 255,
				                        'transparant' => false,
				                    ],
				                   	'width' => 400,
                    				'margin' => [
				                        'x' => 0,
				                        'y' => 0,
				                    ],
				                    'target_location' => [
				                        'x' => 150,
				                        'y' => 800,
				                    ]
				    			]
				    		);
    	// assosiate image with the template content
    	$content->contentable()->associate($username_text)->save();
    }
}
