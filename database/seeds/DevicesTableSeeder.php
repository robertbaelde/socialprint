<?php

use Illuminate\Database\Seeder;

class DevicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('devices')->insert([
        	'key' => '1-'.str_random(5),
            'name' => 'Device 1',
            'note' => '',
            'status' => 'active',
            'created_at' => Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
