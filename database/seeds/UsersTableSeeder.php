<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Robert Baelde',
            'email' => 'robert_baelde@hotmail.com',
            'password' => bcrypt('secret'),
            'level' => 9
        ]);
        DB::table('users')->insert([
            'name' => 'Test Gebruiker 1',
            'email' => 'test@hotmail.com',
            'password' => bcrypt('test123'),
            'level' => 0
        ]);
    }
}
