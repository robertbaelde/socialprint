<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrinterQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('printer_queues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('setup_id');
            $table->integer('printer_id')->default(0);

            $table->integer('queueable_id'); 
            $table->string('queueable_type'); // bv. App\Instagramfeed

            $table->enum('status', ['placed', 'declined','ready_for_printer','printing', 'printed', 'cancled', 'error'])->default('placed');

            $table->text('filepath');
            $table->text('userdata');
            $table->integer('feed_user_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('printer_queues');
    }
}
