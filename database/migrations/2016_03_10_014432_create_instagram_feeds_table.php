<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_feeds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('setup_id');
            $table->integer('template_id');


            $table->string('name');

            $table->string('hashtag');
            $table->bigInteger('min_tag_id')->default(0);

            $table->text('accessToken');

            $table->string('lat');
            $table->string('lng');

            $table->integer('location_id');

            $table->enum('method', ['hashtag', 'location_lat_lng', 'location_id'])->default('hashtag');

            $table->enum('status', ['enabled', 'disabled'])->default('enabled');


            $table->datetime('last_api_call');
            $table->string('last_response');

             $table->text('note');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instagram_feeds');
    }
}
