<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_errors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('device_id');

            $table->enum('type', ['recursive', 'one_time_error']);
            
            $table->string('key');
            $table->text('description');
            $table->string('count')->default(0);

            $table->datetime('last_occurence');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('device_errors');
    }
}
