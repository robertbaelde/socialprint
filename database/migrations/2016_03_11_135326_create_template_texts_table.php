<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        Schema::create('template_texts', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text');
            $table->text('font_data');
            $table->text('background_color');
            $table->integer('width');
            $table->text('margin');
            $table->text('target_location');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('template_texts');
    }
}
