<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
	protected $fillable = ['user_id','name','note'];
     
    public function content()
    {
        return $this->hasMany('App\TemplateContent');
    }
    public function GetContentarrayAttribute($value)
    {
    	// load all content
    	$content_array = [];
    	$contents = $this->content()->orderBy('order')->get();
    	foreach ($contents as $content) {
    		$content_array[] = [
    			'name' => $content->name,
    			'type' => $content->type,
    			'data' => $content->contentable->toArray(),
    		];
    	}
    	return $content_array;
    }
}
