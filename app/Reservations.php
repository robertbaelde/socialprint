<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservations extends Model
{
	protected $table = 'device_reservations';
	protected $fillable = ['user_id','device_id','from','to'];

	protected $dates = ['updated_at', 'created_at'];
	// 
	
}
