<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Services\PhotoGenerator;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ModifyImage extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $image;
    protected $feed;
    protected $type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    public function __construct($image, $feed, $type)
    {
        $this->image = $image;
        $this->feed = $feed;
        $this->type = $type; //instagram, 
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $font = storage_path('app/frame/fonts/Lato-Bold.ttf');
        $font_italic = storage_path('app/frame/fonts/Lato-Italic.ttf');
        switch ($this->type) {
            case 'instagram':
                $dataReplace = array(
                    '[[instagram.photo]]' => $this->image->images->standard_resolution->url,
                    '[[instagram.user.profilepicture]]' => $this->image->caption->from->profile_picture,
                    '[[instagram.description]]' => $this->image->caption->text,
                    '[[instagram.user.username]]' => $this->image->caption->from->username,
                );
                break;
            
            default:
                # code...
                break;
        }
        
        
        $content_array = $this->feed->template->contentarray;
        // Do data replace
        array_walk_recursive($content_array, function(&$parameter, $key, $dataReplace){
            $parameter = str_replace(
                array_keys($dataReplace), 
                array_values($dataReplace), 
                $parameter
            );
        }, $dataReplace);

        $Photo = new PhotoGenerator(Storage::get('frame/background.jpg'));
       
        foreach ($content_array as $content) {
            switch ($content['type']) {
                case 'image':
                    $Photo->addImage($content['data']);
                    // $this->addImage($frame, $content['data']);      
                    break;
                case 'text':
                    $Photo->addText($content['data']);
                    // $this->addText($frame, $content['data']);      
                    break;
                default:
                    break;
            }
        }
        $img_path = '/generated/'
        .str_slug($this->feed->setup->id.'-'.$this->feed->setup->name)
        .'/feeds/'
        .str_slug($this->feed->id.'-'.$this->feed->name).
        '/generated_images';
        // storage_path()
        // ('app/modified_images/'.$this->feed->setup->id.'/feeds/'.$this->feed->id.'/images/');
        $img_filename = str_random(30).'.jpg';
        $Photo->save(public_path().$img_path, $img_filename);

        // save to printqueue
        // dd($this->feed->setup->name);
        
        $queue = $this->feed->setup->queue()->create([
                'filepath' => $img_path.'/'.$img_filename,
                'userdata' => 0,
                'feed_user_id' => 0,
                'status' => $this->feed->setup->initialstatus,
            ]);
        // attach feed to queue object
        $queue->queueable()->associate($this->feed);
        $queue->save();
    }
         
}
