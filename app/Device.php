<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{
	use SoftDeletes;

	protected $fillable = ['key','name','note','status', 'access_token'];

	protected $dates = ['deleted_at', 'created_at'];

	public static function Validation($method = 'create')
	{
		$rules = [
			'key' => '',
			'name' => 'required',
			'note' => '',
			'status' => 'in:active,disabled'
    	];
		if($method == 'create'){
			$rules['key'] = 'required|unique:devices,key';
			$rules['name'] .= '|unique:devices,name,NULL,id,deleted_at,NULL';
		}
		return $rules;
	}
	public function isAvailable($from, $to)
	{
		// dd($this->reservations()->where('from', '<=', $from)->where('to', '>=', $to)->first());
		// check if this device is available in the specified period
		if(!$this->reservations()->where('from', '<=', $from)->where('to', '>=', $to)->first()){
			return true;
		}
		return false;
	}
	public function logError($key, $description, $type)
	{
		// see if error already exists
		$error = $this->errors()->where('key', $key)->first();
		if(!$error)
		{
			// error does not exists. so create one
			$error = $this->errors()->create([
					'key' => $key,
					'description' => $description,
					'type' => $type,
				]);
		}
		// update error
		$error->increment('count');
		$error->last_occurence = Carbon::now()->toDateTimeString();
		$error->save();
		return $error->timeout;
	}
	public function clearErrors($type = false){
		if($type==false){
			return $this->errors()->delete();
		}
		return $this->errors()->where('type', $type)->delete();

	}

	//	Relationships
	public function activity()
    {
        return $this->morphMany('App\Activity', 'activable');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'device_reservations')->withTimestamps();
    }
    public function reservations()
    {
		return $this->hasMany('App\Reservations');
    }
    public function setup()
    {
    	return $this->belongsTo('App\Setup');
    }
    public function errors()
    {
    	return $this->hasMany('App\DeviceErrors');
    }

}
