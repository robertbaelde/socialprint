<?php

namespace App\Console\Commands;

use App\Services\Feeds;
use Illuminate\Console\Command;

class PullFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feeds:pull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load recent media from all live feeds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Feeds::pull();
    }
}
