<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateContent extends Model
{
    protected $fillable = ['name','type','order'];

    protected $TransformTypes = [
    	'App\TemplateImage' => 'image',
    	'App\TemplateText' => 'text',
    ];
    public function contentable()
    {
        return $this->morphTo();
    }
    public function getTypeAttribute()
    {
    	return $this->TransformTypes[$this->contentable_type];
    }
}
