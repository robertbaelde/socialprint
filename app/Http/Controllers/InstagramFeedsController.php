<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\InstagramFeed;
use App\Setup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class InstagramFeedsController extends Controller
{
    protected $setup;

    public function __construct()
    {
        // dd(Route::current());
        $setup_id = Route::current()->getParameter('setup');
        $this->setup = Setup::where('id', $setup_id)->where('user_id', Auth::user()->id)->first();
        if(!$this->setup){
            abort(404);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd($this->setup->instagramfeed());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.instafeed.create', ['setup' => $this->setup]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, InstagramFeed::validation());

        $feed = $this->setup->instagramfeed()->create($request->all());

        // authenticate Instagram account        
        return redirect('setup/'.$this->setup->id.'/instafeed/'.$feed->id.'/authorize')->with('success', 'Feed '.$request->get('name').' created successfull');
        // return redirect('setup/'.$this->setup->id.'/instafeed/'.$feed->id)->with('success', 'Feed '.$request->get('name').' created successfull');
    }
   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($setup_id, $feed_id)
    {
        // make test request
        return view('dashboard.instafeed.show', ['setup' => $this->setup, 'instafeed' => $this->setup->instagramfeed()->findOrFail($feed_id)]);
    }
    /**
     * Authorize an instagram account for this feed.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function authorizeInstagramAccount(Request $request, $setup_id, $feed_id)
    {
        if($request->input('AccessToken', false) == false){

        // $request->session()->put('feedToAuthorizeId', $feed_id);
        $request->session()->put('InstaAuthenticateRedirectUrl', url('setup/'.$this->setup->id.'/instafeed/'.$feed_id.'/authorize'));
// 'setup/'.$this->setup->id.'/instafeed/'.$feed_id
        return redirect('instagramlogin');
        }
        // AccessToken present, save it
        $feed = InstagramFeed::findOrFail($feed_id);
        $feed->accessToken = $request->input('AccessToken');
        $feed->save();

        return redirect('setup/'.$this->setup->id.'/instafeed/'.$feed->id)->with('success', 'Instagram account authorized successfull');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
