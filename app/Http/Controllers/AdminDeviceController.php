<?php

namespace App\Http\Controllers;

use App\Device;
use App\Http\Requests;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminDeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->isAdmin()){
            return back()->with('warning', 'not allowed');
        }
        return view('dashboard.device.index', ['devices' => Device::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.device.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Device::validation());

        $device = Device::create(array_merge($request->all(), ['access_token' => str_random(40)]));
        
        return redirect('device/'.$device->id)->with('success', 'Device '.$request->get('name').' created successfull');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $device = Device::findOrFail((int)$id);
        
        return view('dashboard.device.show', ['device' => $device]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $device = Device::findOrFail((int)$id);
        
        return view('dashboard.device.edit', ['device' => $device]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Device::validation('update'));

        $device = Device::findOrFail($id);

        $device->name = $request->input('name', $device->name);
        $device->note = $request->input('note', $device->note);
        $device->status = $request->input('status', $device->status);
        
        $device->save();

        return redirect('device/'.$id)->with('success', 'Device '.$request->input('name').' updated successfull');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $device = Device::find($id);

        $request->session()->flash('success', 'Device '.$device->name.' successfull deleted');

        $device->delete();

        return 'success';
    }
    public function reservation(Request $request, $id)
    {
        $device = Device::findOrFail($id);

        $this->validate($request, [
            'user_id' => 'required|integer|exists:users,id',
            'from' => 'required|date',
            'to' => 'required|date',
        ]);

        $user = User::findOrFail($request->input('user_id'));

        $from = Carbon::createFromFormat('d-m-Y', $request->input('from'));
        $to = Carbon::createFromFormat('d-m-Y', $request->input('to'));
        // check if device is not yet reserved for the required period
        if(!$device->isAvailable($from->toDateString(), $to->toDateString())){
            return redirect('device/'.$id)->with('warning', $device->name.' not available at those dates')->withInput();
        }
        $device->users()->attach($request->input('user_id'), [
            'from' => $from->toDateString(),
            'to' => $to->toDateString(),
            'note' => $request->input('note'),
        ]);
        $device->activity()->create([
            'title' => 'Device connected to '.$user->name,
            'body' => $request->input('note'),
            'footer' => 'from '.$from->toFormattedDateString().' to '.$to->toFormattedDateString(),
            'icon' => 'fa fa-calendar',
        ]);
        return redirect('device/'.$id)->with('success', 'Reservation for '.$device->name.' successfull');
    }
}
