<?php

namespace App\Http\Controllers;

use App\Device;
use App\Http\Requests;
use App\PrinterQueue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DeviceApiController extends Controller
{
	protected $default_timeout = 300; // secconds
    public function ping()
    {
    	return response()->json(['success' => true, 'data' => 'pong']);
    }
    public function getjob(Request $request)
    {
		$device = $this->loadDevice($request);
		if(!$device instanceof Device){
			return $device;
		}
		// Device is assigned, and enabled! lets fetch some jobs!
		$job = $device->setup->getJob($device->id);
		if(!$job){
			return response()->json(['success' => true, 'timeout' => 1 ,'data' => ['image_url' => false]]);
		}
		return response()->json(['success' => true, 'timeout' => 1 ,'data' => ['id' => $job->id, 'image_url' => asset($job->filepath)]]);
    }
    public function finishdJob(Request $request)
    {
    	$device = $this->loadDevice($request->input('device_key'), $request->input('access_token'));
		if(!$device instanceof Device){
			return $device;
		}
		return response()->json(['success' => true, 'timeout' => 60]);
    }

    private function loadDevice($request)
    {
    	$device = Device::where('key', $request->input('device_key'))->first();
    	if(!$device){
    		return response()->json(['success' => false, 'error' => 'device not found', 'timeout' => $this->default_timeout]);
    	}
    	// set job as finishd, no setup needed for that
    	if($request->input('finishd_job_id', false) !== false){
			PrinterQueue::jobFinishd($device->id, $request->input('finishd_job_id'));
    	}
    	if($device->setup_id == 0){
    		$timeout = $device->logError('not_assigned_to_setup', 'The device '.$device->name.' is not assigned to a setup', 'recursive');
    		return response()->json(['success' => false, 'error' => 'device not assigned to setup', 'timeout' => $timeout]);
    	}
    	if($device->status == 'disabled'){
    		$timeout = $device->logError('disabled', 'The device '.$device->name.' is disabled', 'recursive');
    		return response()->json(['success' => false, 'error' => 'device is disabled', 'timeout' => $timeout]);
    	}
		$device->clearErrors('recursive');
		return $device;
    }
}
