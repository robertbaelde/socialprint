<?php

namespace App\Http\Controllers;

use App\Device;
use App\Http\Requests;
use App\Setup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserDeviceController extends Controller
{
    public function assign($setup_id, $device_id)
    {
    	// get setup
    	$setup = Setup::where('user_id', Auth::user()->id)->findOrFail($setup_id);
    	$device = Device::findOrFail($device_id);
    	if(!Auth::user()->hasDevice($device->id)){
    		return back()->with('warning', 'Device not owned by user');
    	}
    	$device->setup()->associate($setup);
    	$device->save();
    	
    	return back()->with('success', 'Device '.$device->name.' assigned');
    }
    public function resign($setup_id, $device_id)
    {
    	$setup = Setup::where('user_id', Auth::user()->id)->findOrFail($setup_id);
    	$device = $setup->device->find($device_id);
    	if(!$device){
    		return back()->with('warning', 'Device not assigned to this setup');
    	}
    	$device->setup()->dissociate();
    	$device->save();
    	
    	return back()->with('success', 'Device '.$device->name.' resigned');
    }
    public function activate($setup_id, $device_id)
    {
    	$setup = Setup::where('user_id', Auth::user()->id)->findOrFail($setup_id);
    	$device = $setup->device->find($device_id);
    	if(!$device){
    		return back()->with('warning', 'Device not assigned to this setup');
    	}
    	$device->status = 'active';
    	$device->save();

    	return back()->with('success', 'Device '.$device->name.' status changed to disabled');


    }
    public function disable($setup_id, $device_id)
    {
    	$setup = Setup::where('user_id', Auth::user()->id)->findOrFail($setup_id);
    	$device = $setup->device->find($device_id);
    	if(!$device){
    		return back()->with('warning', 'Device not assigned to this setup');
    	}
    	$device->status = 'disabled';
    	$device->save();

    	return back()->with('success', 'Device '.$device->name.' status changed to active');


    }

}
