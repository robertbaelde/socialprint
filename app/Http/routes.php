<?php

use App\InstagramFeed;
use Illuminate\Support\Facades\Session;
use Vinkla\Instagram\Facades\Instagram;
use Vinkla\Instagram\InstagramManager;

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

  


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::get('test', function(InstagramManager $instagram){

	$instagram->setAccessToken('19463805.d3413a1.5685374aa5294610a72348ac78c2c127');

	dd($instagram->getLocationMedia(79212));
});
Route::get('device_emulator', function(){
	return view('deviceemulator');
});
Route::get('wall', function(){
	return view('wall');
});
Route::get('wall2', function(){
	return view('wall2');
});
// Device api routes, all routes must have an device_key and a access_token

Route::group(['middleware' => ['deviceapi']], function () {
	Route::get('api/v1/device/ping', 'DeviceApiController@ping'); 
	Route::get('api/v1/device/getjob', 'DeviceApiController@getjob'); 
	Route::get('api/v2/device/finishdJob', 'DeviceApiController@finishdJob');
});

// 
Route::group(['middleware' => ['web']], function () {
	Route::controller('auth', 'Auth\AuthController');
	Route::get('login', function(){
		return redirect('auth/login');
	});
});
Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/', function () {
	    return view('dashboard.template');
	});
	Route::get('logout', function(){
		Auth::logout();
		return redirect('/');
	});

	// AdminDeviceController routes:
	Route::post('admin/device/{device}/reservation', 'AdminDeviceController@reservation');
	Route::resource('admin/device', 'AdminDeviceController');



	// Setupcontroller routes
	Route::resource('setup', 'SetupController');

	// Instagram feeds routes
	Route::get('setup/{setup}/instafeed/{feed}/authorize', 'InstagramFeedsController@authorizeInstagramAccount');
	// user device assign / resign
	Route::get('setup/{setup}/device/{device}/assign', 'UserDeviceController@assign');
	Route::get('setup/{setup}/device/{device}/resign', 'UserDeviceController@resign');
	// activate / disable device
	Route::get('setup/{setup}/device/{device}/activate', 'UserDeviceController@activate');
	Route::get('setup/{setup}/device/{device}/disable', 'UserDeviceController@disable');
	


	Route::resource('setup/{setup}/instafeed', 'InstagramFeedsController');




	// instagram api routes
	Route::get('instagramlogin', function(InstagramManager $instagram){
		// To get code:
		if(\Request::input('code', false) == false){
			return redirect($instagram->getLoginUrl());		
		}
		
        // Request the access token.
        $data = Instagram::getOAuthToken(\Request::input('code'));
        if (Session::has('InstaAuthenticateRedirectUrl')) {
        	return redirect(Session::pull('InstaAuthenticateRedirectUrl').'?AccessToken='.$data->access_token);
		}
		// no session present
		return redirect('/');
        // Set the access token with $data object.
        Instagram::setAccessToken('19463805.d3413a1.5685374aa5294610a72348ac78c2c127');

        // We're done here - how easy was that, it just works!
        dd(Instagram::getTagMedia('leesvoer'));
	});
});
