<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;

class DeviceApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validator = Validator::make($request->all(), [
            'device_key' => 'required|exists:devices,key,access_token,'.$request->input('access_token', 'no_token'),
        ]);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => 'Device not found, or invalid access token', 'timeout' => 10]);
        }
        
        return $next($request);
    }
}
