<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceErrors extends Model
{
	protected $fillable =['key','description','last_occurence'];
    
    public function getTimeoutAttribute()
    {
    	if($this->count < 5){
    		return 10;
    	}
    	if($this->count > 30){
    		return 300;
    	}
    	return 10*$this->count;
    }
}
