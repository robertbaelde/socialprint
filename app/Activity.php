<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
	protected $fillable = ['title','body','footer','icon'];

	protected $dates = ['created_at'];

    public function activable()
    {
        return $this->morphTo();
    }
}
