<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramFeed extends Model
{
    protected $fillable = ['name','hashtag','lat','lng','locaton_id','method', 'note', 'template_id'];
	protected $dates = ['created_at'];

	public static function Validation($method = 'create')
	{
		$rules = [
			'name' => 'required',
			'note' => '',
			'method' => 'in:hashtag,location_lat_lng,location_id'
    	];
		if($method == 'create'){
			
		}
		return $rules;
	}

	public function scopeActive($query)
    {
        return $query->where('status','enabled');
    }
    public function template()
    {
        return $this->belongsTo('App\Template');
    }
    public function setup()
    {
        return $this->belongsTo('App\Setup');
    }
    
}
