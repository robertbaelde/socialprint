<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Setup extends Model
{
    use SoftDeletes;

	protected $fillable = ['key','name','note','status'];

	protected $dates = ['deleted_at', 'created_at'];

	public static function Validation($method = 'create')
	{
		$rules = [
			'name' => 'required',
			'note' => '',
    	];
		if($method == 'create'){
		}
		return $rules;
	}
    public function getInitialstatusAttribute()
    {
        return 'ready_for_printer';
    }
	public function instagramfeed()
    {
       return $this->hasMany('App\InstagramFeed');
    }
    public function queue()
    {
        return $this->hasMany('App\PrinterQueue');
    }
    public function device()
    {
    	return $this->hasMany('App\Device');
    }
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    // jobs processing
    public function getJob($device_id)
    {
        DB::beginTransaction();
        // load job
        // dd($this->queue->where('status', 'ready_for_printer')->first());
        $job = $this->queue()->where('status', 'ready_for_printer')->orderBy('created_at', 'asc')->lockForUpdate()->first();
        if(!$job){
            DB::rollBack();
            return false;
        }
        $job->status = 'printing';
        $job->printer_id = $device_id;
        $job->save();
        DB::commit();

        return $job;
    }
    public function jobFinishd($device_id, $job_id)
    {
        DB::beginTransaction();
        $job = $this->queue()->where('id', $job_id)->where('printer_id', $device_id)->where('status', 'printing')->first();
        $job->status = 'printed';
        $job->save();
        DB::commit();
        return true;
    }
}
