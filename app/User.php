<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        if($this->level == 9){
            return true;        
        }
        return false;
    }

    public function devices()
    {
        return $this->belongsToMany('App\Device', 'device_reservations')
                    ->withTimestamps()
                    ->where('from', '<=', Carbon::now()->toDateString())
                    ->where('to', '>=', Carbon::now()->toDateString());
    }
    public function hasDevice($device_id)
    {
        $result = $this->belongsToMany('App\Device', 'device_reservations')
                    ->withTimestamps()
                    ->where('from', '<=', Carbon::now()->toDateString())
                    ->where('to', '>=', Carbon::now()->toDateString())
                    ->where('device_id', $device_id)->first();
        if(!$result){
            return false;
        }
        return true;
    }


    public function setups()
    {
       return $this->hasMany('App\Setup');
    }
    public function instafeeds()
    {
       return $this->hasMany('App\Setup');
    }
}
