<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateImage extends Model
{
    protected $dates = ['updated_at', 'created_at'];

    protected $casts = [
        'resolution' => 'array',
		'target_location' => 'array',
    ];
    public function content()
    {
        return $this->morphMany('App\TemplateContent', 'contentable');
    }
    public function getDataAttribute()
    {
    	
    }
}
