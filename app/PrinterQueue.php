<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PrinterQueue extends Model
{
    protected $fillable = ['status','filepath','userdata','feed_user_id'];
    
    public function queueable()
    {
        return $this->morphTo();
    }
    public static function jobFinishd($device_id, $job_id)
    {
        DB::beginTransaction();
        $job = PrinterQueue::where('id', (int)$job_id)->where('printer_id', $device_id)->where('status', 'printing')->first();
        if(!$job){    
            DB::rollBack();       
        	return false;
        }
        $job->status = 'printed';
        $job->save();
        DB::commit();
        return true;
    }
}
