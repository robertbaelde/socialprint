<?php namespace App\Services;

use Illuminate\Support\Facades\App;
/**
* Feeds class, handling updating
*/

class Feeds
{
	protected static $feed_classes = ['App\Services\InstagramFeedProcessor'];

	public static function pull()
	{
		// load feeds that are live
		foreach (static::$feed_classes as $class) {
			$feed = App::make($class)->pull();
		}
		return true;
	}
	
}