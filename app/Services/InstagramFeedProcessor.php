<?php namespace App\Services;

use App\InstagramFeed;
use App\Jobs\ModifyImage;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Storage;
use Vinkla\Instagram\InstagramManager;

class InstagramFeedProcessor
{
	protected $feeds;
	protected $instagram;
	protected $photoarray = [];

	protected $limit = 50;

	use DispatchesJobs;

	function __construct(InstagramManager $instagram)
	{
		$this->feeds = InstagramFeed::active()->get();
		$this->instagram = $instagram;
	}
	public function pull()
	{
		// get active instagram feeds and pull them
		// return ['foo' => 'bar'];
		foreach ($this->feeds as $feed) {
			$this->setAuthenticationKey($feed);
			switch ($feed->method) {
				case 'hashtag':
					$data = $this->getByHashtag($feed);
					break;
				case 'location_lat_lng':
					$data = $this->getByLatLong($feed->lat, $feed->lng);
					break;
				case 'location_id':
					$data = $this->getByLocationId($feed);
					break;
				default:
					break;

			}
			$feed->last_api_call = Carbon::now()->toDateTimeString();
			$feed->save();

			$this->ProcessImages($data, $feed);
		}
	}
	private function setAuthenticationKey($feed)
	{
		$this->instagram->setAccessToken($feed->accessToken);
	}
	private function getByHashtag($feed)
	{
		$response = $this->instagram->getTagMedia($feed->hashtag, $this->limit, $feed->min_tag_id);
		// save min_tag_id
		if(count($response->data) > 0){
			// $feed->min_tag_id = $response->pagination->min_tag_id;
		}
		

		return $response->data;
	}
	private function getByLatLong($lat, $lng)
	{
		$distance = 5000;
		return $this->instagram->searchMedia($lat, $lng, $distance);
	}
	private function getByLocationId($feed)
	{
		// dd($feed->locaton_id);
		$response = $this->instagram->getLocationMedia($feed->location_id);
		return $response->data;
	}

	private function ProcessImages($images, $feed)
	{
		foreach ($images as $image) {
			// save to array, to prevent a photo with two same hashtags get printed out twice
			if(!array_key_exists($feed->setup_id, $this->photoarray)){
				$this->photoarray[$feed->setup_id] = [];
			}
			if(!in_array($image->id, $this->photoarray[$feed->setup_id])){
				// add id to photo array, to prevent printing twice
				$this->photoarray[$feed->setup_id][] = $image->id;

				$this->dispatch(new ModifyImage($image, $feed, 'instagram'));
			}
		}
	}

}
