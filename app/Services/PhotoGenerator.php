<?php namespace App\Services;

use App\Services\ImageProcessor;



class PhotoGenerator
{
	protected $frame;
	function __construct($frame)
	{
		$this->frame = imagecreatefromstring($frame);
		return $this;
	}

	public function addImage($data)
	{
		ImageProcessor::addImage($this->frame, $data);
		return $this;
	}
	public function addText($data)
	{
		ImageProcessor::addText($this->frame, $data);
		return $this;
	}
	public function save($path, $filename){
		if (!file_exists($path)) {
		    mkdir($path, 0777, true);
		}
        imagejpeg($this->frame, $path.'/'.$filename);
        return $this;
	}
}