<?php namespace App\Services;

class ImageProcessor
{

	public static function generate_text($text, $font_data, $background_color_data, $width, $margin)
    {
        //explode text by words
        $text_a = explode(' ', $text);
        $text_new = '';
        foreach($text_a as $word){
            //Create a new text, add the word, and calculate the parameters of the text
            $box = imagettfbbox($font_data['size'], 0, $font_data['font'], $text_new.' '.$word);
            //if the line fits to the specified width, then add the word with a space, if not then add word with new line
            if($box[2] > $width - $margin['x']*2){
                $text_new .= "\n".$word;
            } else {
                $text_new .= " ".$word;
            }
        }
        //trip spaces
        $text_new = trim($text_new);
        //new text box parameters
        $box = imagettfbbox($font_data['size'], 0, $font_data['font'], $text_new);
        //new text height
        $height = $box[1] + $font_data['size'] + $margin['y'] * 2;
         
        //create image
        $im = imagecreatetruecolor($width, $height);
         
        //create colors
        $background_color = imagecolorallocate($im, 255, 255, 255);
        $font_color = imagecolorallocate($im, 0, 0, 0);
        //color image
        imagefilledrectangle($im, 0, 0, $width, $height, $background_color);
         
        //add text to image
        imagettftext($im, $font_data['size'], 0, $margin['x'], $font_data['size']+$margin['y'], $font_color, $font_data['font'], $text_new);
         
        return [
            'image' => $im,
            'width' => $width,
            'height' => $height,
        ];
    }
    public static function addText($frame, $text_data)
    {
        $rendered_text = self::generate_text($text_data['text'], $text_data['font_data'], $text_data['background_color'], $text_data['width'], $text_data['margin']);
        imagecopymerge($frame, $rendered_text['image'], $text_data['target_location']['x'], $text_data['target_location']['y'], 0, 0, $rendered_text['width'], $rendered_text['height'], 100);
        imagedestroy($rendered_text['image']);
    }
    public static function addImage($frame, $image_data)
    {
        $image = imagecreatefromstring(file_get_contents($image_data['source']));
        $image = imagescale($image, $image_data['resolution']['x'], $image_data['resolution']['y'], $image_data['resolution']['mode']);
        imagecopymerge($frame, $image, $image_data['target_location']['x'], $image_data['target_location']['y'], 0, 0, $image_data['resolution']['x'], imagesy($image), 100);
        //frees any memory associated with image
        imagedestroy($image);
    }
	
}