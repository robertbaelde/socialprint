<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateText extends Model
{
    protected $dates = ['updated_at', 'created_at'];

    protected $casts = [
		'font_data' => 'array', 
		'background_color' => 'array', 
		'margin' => 'array', 
		'target_location' => 'array', 
    ];
    
    public function content()
    {
        return $this->morphMany('App\TemplateContent', 'contentable');
    }
}
